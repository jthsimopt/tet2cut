function [fi,fix,fiy,fiz,vol] = basetetP2(X,xc,yc,zc)
    % x y z can be scalar or vectors.
    x = X(:,1); y = X(:,2);  z = X(:,3); 
    A = zeros(10);
    for i=1:10
        A(i,:) = [1,xc(i),yc(i),zc(i),xc(i)*yc(i),xc(i)*zc(i),yc(i)*zc(i),xc(i)^2,yc(i)^2,zc(i)^2];
    end
    
    x=x(:);y=y(:);z=z(:); 
    b = [x*0+1,x,y,z,x.*y,x.*z,y.*z,x.^2,y.^2,z.^2];
    C = A\eye(10); % Constants per node
    
    dbdx = [x*0,x*0+1,x*0,x*0,y,z,x*0,2*x,0*x,0*x];
    dbdy = [x*0,x*0,y*0+1,x*0,x,x*0,z,0*x,2*y,0*z];
    dbdz = [x*0,x*0,y*0,z*0+1,0*x,x,y,0*x,0*y,2*z];
    
    fi = C'*b';
    fix = C'*dbdx';
    fiy = C'*dbdy';
    fiz = C'*dbdz';
    
    v=cross([xc(2)-xc(1),yc(2)-yc(1),zc(2)-zc(1)],[xc(3)-xc(1),yc(3)-yc(1),zc(3)-zc(1)]);
    vol=abs(dot(v,[xc(4)-xc(1),yc(4)-yc(1),zc(4)-zc(1)]))/6;
    
end