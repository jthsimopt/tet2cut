clear
clc
close all

%% Required files
matlabDir = UpDir(pwd,2);
addpath(fullfile(matlabDir,'Mesh','Tet2MeshDev','Tet2Mesh'))
addpath(fullfile(pwd,'TetP2CutSurface'))


%% Parameters
ne = 3;
R = 0.49;

global gc1 H
gc1 = 0;
H.c = 0;

%% P2 Tet mesh
x0 = 0; x1 = 1;
y0 = 0; y1 = 1;
z0 = 0; z1 = 1;
nxe = ne; nye = ne; nze = ne;

mesh = Tet2Mesh(x0,x1,y0,y1,z0,z1,nxe,nye,nze);

% vizP1Mesh(mesh.T(:,1:4),mesh.P);
% title('P1 elements')
% 
xfigure; light
vizP2Faces(mesh.faces,mesh.P,2);
title('P2 elements')

%% Surface function
surfaceFunction = @(x,y,z) ((x-0).^2 + (y-0).^2 + (z-0).^2).^0.5-R; %Spehre

%% Extract surface
tet1 = mesh.T(:,1:4);
tet2 = mesh.T;
Elements = mesh.Elements;
P = mesh.P;
xnod = P(:,1); ynod = P(:,2); znod = P(:,3);
phi = surfaceFunction(P(:,1),P(:,2),P(:,3));
phi0Map = phi == 0;
phi(phi0Map) = phi(phi0Map)+sqrt(eps) ;

PHI = phi(tet2);
cutEle = find(min(PHI,[],2).* max(PHI,[],2)<0);
nele = size(tet1,1);

facesP2 = mesh.tet2Faces(cutEle);
xfigure;
vizP2Faces(facesP2,P,2);
title('Cut P2 elements')

poldegree = 2;
tol = 1e-9;

rs = [0,0;1,0;0,1];
rr = rs(:,1); ss = rs(:,2);
[gcx,gcy,gw]=trigauc(rs(:,1),rs(:,2),poldegree);


elesize = (max(P(:,1))-min(P(:,1)))/(nele/6)^(1/3);
th = sqrt(eps) * elesize;

nCutEle = length(cutEle);
% cutEle = [3,6,58]'
for iel = cutEle'
    
    iv = tet2(iel,:);
    faces = Elements(iel).faces;
    faces3 = faces(:,1:3);
    
    PHIF = phi(faces);
    cutFaces = faces(min(PHIF,[],2).*max(PHIF,[],2) < 0, :);
    %% Find Edge cut points on tet
    [data, EE] = EdgeCutPoints(P, cutFaces, phi, tol, iel, Elements, tet2);
    
    %% Merge and orient the datapoints
    [tt,iP0] = MergeAndOrientDataPoints(data, EE);
    nP = length(tt);
    
%     xfigure(45)
%     patch('Faces',tt(1:nP/2),'Vertices',PX,'FaceColor','r')
%     patch('Faces',tt(1+nP/2:nP),'Vertices',PX,'FaceColor','y')
%     text(PX(1:nP/2,1),PX(1:nP/2,2),PX(1:nP/2,3),cellstr(num2str( (1:nP/2)' )),'BackgroundColor','r')
%     text(PX(1+nP/2:nP,1),PX(1+nP/2:nP,2),PX(1+nP/2:nP,3),cellstr(num2str( (1+nP/2:nP)' )),'BackgroundColor','y')
    
    
    xc = P(iv,1); yc = P(iv,2); zc = P(iv,3);
    PHI = phi(iv);
%     text(xc,yc,zc,cellstr(num2str( PHI )),'BackgroundColor','w')
     
    %% If Serendipity, create midpoint in order to split the serendipity into triangles
    if nP == 8
        diagEdge = [1,3];
        p12 = iP0(diagEdge,:);
        p1 = p12(1,:); p2 = p12(2,:);
%         ip = sum(p12,1)/2;
        ip = mean(iP0,1);
%         ip = sum(PX(diagEdge,:),1)/2; %Midpoint is the starting point for the newton method
%         nip = 51; % Needs to be an odd number >= 3
%         ips = [linspace(p1(1),p2(1),nip)', linspace(p1(2),p2(2),nip)', linspace(p1(3),p2(3),nip)'];
%         ips([1,end],:) = []; nip = nip-2;
%         a = (1+1):2:nip; b = 1:2:nip;
%         inds = [a(end):-2:a(1),b];
%         [~,inds]=sort(inds);
%         ips = ips(inds,:);
%         text(ips(inds,1),ips(inds,2),ips(inds,3),cellstr(num2str( (1:nip).' )),'BackgroundColor','w')
        %% Compute normal
        tetNormal = TetElementNormal(faces3,phi,xnod,ynod,znod);
        
%         for iips = 1:nip
%             ip = ips(iips,:);
%             %% Compute line - triangle intersections
%             intersectPoints = LineTrianglesIntersection(ip,tetNormal,elesize,th,faces3,P,tet2,iel,phi);
%             plot3(intersectPoints(:,1),intersectPoints(:,2),intersectPoints(:,3),'k-')
% %             ip = ip + (intersectPoints(2,:)-intersectPoints(1,:));
%             plot3(ip(1),ip(2),ip(3),'c*')
%             
%             
%             %% Find cut point along line
%             rP = FindRootOnLine(ip,intersectPoints,P,tet2,phi,tol,th,iel);
%             xfigure(45)
%             plot3(rP(1),rP(2),rP(3),'kx')            
%             1;
%         end
%         tic
        rP = FindRootAlongDir(ip,P,tet2,phi,th,iel);
%         toc
        
    %     text(xc(1:4),yc(1:4),zc(1:4),cellstr(num2str(PHI(1:4))),'BackgroundColor','w')
        
        %% Split into triangles
        1;
        tt = [1,2,3,5,6,9;...
            1,3,4,9,7,8];
        iP = [iP0;rP];
        
    else
        iP = iP0;
    end
    %% Viz
    if H.c == 0
        xfigure(3);
%         H.hpf = patch('Faces',faces3,'Vertices',P,'FaceColor','b'); axis equal off tight; hold on;
%         H.hpf.FaceAlpha = 0.1;
%         H.hpf2 = patch('Faces',faces3,'Vertices',P,'FaceColor','none');
%         VizIsoLines(tet2,P,iel,faces,phi)
        H.c = H.c+1;
    else
%         H.hpf.Faces = faces3; 
%         H.hpf2 = patch('Faces',faces3,'Vertices',P,'FaceColor','none');
        H.c = H.c+1;
    end
    if nP==8
%         vizP2Faces(tt,iP,10,'FaceColor','c','InnerEdgeColor',[0,1,1]*0.9);
        vizP2Faces(tt,iP,10,'FaceColor','c','InnerEdgeColor','none');
%         plot3(iP(1:end-1,1),iP(1:end-1,2),iP(1:end-1,3),'ko','MarkerFaceColor','k') 
%         plot3(iP(end,1),iP(end,2),iP(end,3),'ko','MarkerFaceColor','r') 
    else
%         vizP2Faces(tt,iP,10,'FaceColor','r','InnerEdgeColor',[1,0,0]*0.9);
        vizP2Faces(tt,iP,10,'FaceColor','r','InnerEdgeColor','none');
%         plot3(iP(:,1),iP(:,2),iP(:,3),'ko','MarkerFaceColor','k') 
    end
     
    
    
%     text(xc(:),yc(:),zc(:),cellstr(num2str(PHI(:))),'BackgroundColor','w')
    1;
%     close all
end

function VizIsoLines(tet2,P,iel,faces,phi)
    %% Isolines
    itet = tetP2(tet2,P,iel);
    rs = [0,0;1,0;0,1;0.5,0;0.5,0.5;0,0.5];

    i = 1;
    iFv = faces(i,:);
    PHI= phi(iFv);
    tt = [1,4,6;6,5,3;4,5,6;4,2,5];
    tri = [1,2,3,4,5,6];
    %         [T,X,kk] = P2Interp(tri,[rs,ones(6,1)],4);
    [T,X,UI,kk] = P2Interp(tri,[rs,ones(6,1)*0],PHI,30);

    xfigure(55);
    hp = patch('Faces',T,'Vertices',X,'FaceColor','interp','CData',UI); hold on;
    hp.EdgeColor = 'none';
    E = sort([T(:,1) T(:,2); T(:,2) T(:,3); T(:,3) T(:,1)]')';
    [u,m,n] = unique(E,'rows');
    % determine uniqueness of edges
    [u,m,n] = unique(E,'rows');
    % determine counts for each unique edge
    counts = accumarray(n(:), 1);
    % extract edges that only occurred once
    O = u(counts==1,:);

    % resample W with a grid, THIS IS VERY SLOW if V is large
    [Xr,Yr,Wr] = griddata(X(:,1),X(:,2),UI,unique(X(:,1)),unique(X(:,2))');
    % find all points inside the mesh, THIS IS VERY SLOW if V is large
    IN = inpolygon(Xr,Yr,X(O,1),X(O,2));
    % don't draw points outside the mesh
    Wr(~IN) = NaN;

    [C0,hc] = contour(Xr,Yr,Wr,[0,0]);
    hc.LineColor = 'k';
    hc.LineWidth = 1.5;
    [C,hc] = contour(Xr,Yr,Wr,10);
    hc.LineColor = 'k';
end

function [T,X,UI,kk] = P2Interp(tri,P,U,np)
    nP2ele=size(tri,1);
    nTriEle = nP2ele*np^2;
    
    dx=1/np;
    [X,Y] = meshgrid(0:dx:1,0:dx:1);
    x=X(:);y=Y(:);
    ind=find(y>eps+1-x);
    x(ind)=[];y(ind)=[];

    tt = subTriang(np,x);

    T = zeros(nP2ele*np^2,3);
    X = NaN(nP2ele*length(x)^2,3);
    UI = NaN(size(X,1),1);
    kk = zeros(nP2ele,3*(np+1)-2);

    for iel=1:nP2ele
        iv6=tri(iel,:);
        %         iv3=tri(iel,1:3);
        xc=P(iv6,1);yc=P(iv6,2);zc=P(iv6,3);

        fiP2=[1-3*x+2*x.^2-3*y+4*x.*y+2*y.^2,x.*(-1+2*x),y.*(-1+2*y),-4*x.*(-1+x+y),4*x.*y,-4*y.*(-1+x+y)];
        %         fiP1=[1-x-y,x,y];

        xn=fiP2*xc;
        yn=fiP2*yc;
        zn=fiP2*zc;
        
        un=fiP2*U(iv6);

        % Insert coordinates from iel into X, that will contain all coordinates
        lx=length(xn);
        if iel==1
            X(iel:lx,:)=[xn,yn,zn];
            UI(iel:lx)=un;
        else
            X((iel-1)*lx+1:iel*lx,:)=[xn,yn,zn];
            UI((iel-1)*lx+1:iel*lx)=un;
        end

        %         tt = subTriang(np,x);

        % Global triangle matrix is assembled
        lt=size(tt,1);
        if iel==1
            T(iel:lt,:)=tt;
            tt0=tt;
            tt1=tt0;
        else
            maxtt=max(tt(:));
            tt1=tt1+maxtt;
            lt=size(tt1,1);
            T((iel-1)*lt+1:iel*lt,:)=tt1;
        end

        % Global element edge vector is assembled
        % this is needed to draw the P2 element edges
        indx=find(x == min(x));
        indy=find(y == min(y));
        ns=np+1;
        ns3=zeros(1,ns);
        ns3(1,1)=ns;
        ii=2;
        for i1=ns-1:-1:1
            ns3(1,ii)=ns3(1,ii-1)+i1;
            ii=ii+1;
        end
        k = [indx;ns3(2:end)';indy(end-1:-1:1)];

        k1 = k'+max(kk(:));
        kk(iel,:) = k1;

    end
    X = X(~all(isnan(X),2),:);
    UI = UI(~all(isnan(UI),2),:);
end
function tt = subTriang(n,x)
    nT=length(x);
    ns=n+1;


    ns3=zeros(1,ns);
    ns3(1,1)=ns;
    ii=2;
    for i1=ns-1:-1:1
        ns3(1,ii)=ns3(1,ii-1)+i1;
        ii=ii+1;
    end
    % ns3; %Third side T

    emax=-1;
    for i1=1:n
        emax=emax+2;
    end
    % emax; %number T in last sector
    elements=1:2:emax;
    nelem = sum(elements); %number elements
    T1 = 1:nT; %number T

    si=1;
    ei=ns;
    T2=zeros(ns);
    for i1=1:ns
        T2(i1,i1:ns) = T1(si:ei);
        si=ei+1;
        ei=ns3(i1)+ns-i1;
    end

    tri2=zeros(nelem,3);
    i1=1;
    giel=1;
    for iseq=elements
        if iseq==1
            seq1 = T2(:,[1,2]);
            ind1= seq1==0;
            seq1(ind1)=[];
            seq1 = [seq1(3),seq1(2),seq1(1)];
            tri2(1,:)=seq1;
            giel=2;
        end
        if iseq~=1
            seqi = T2(:,[i1,i1+1]);
            seqi1=seqi(:,1);
            seqi2=seqi(:,2);
            ind1= seqi1==0;
            seqi1(ind1)=[];
            c1=length(seqi1);

            ind2= seqi2==0;
            seqi2(ind2)=[];
            c2=length(seqi2);

            seqi=seqi(:);
            ind1= seqi==0;
            seqi(ind1)=[];
            seqi = seqi';
            a = c2-1;
            b = iseq - a;
            for ai=1:a
                tri2(giel,:) = seqi([ai,ai+c1+1,ai+c1]);
                giel=giel+1;
            end
            for bi=1:b
                tri2(giel,:) = seqi([bi,bi+1,bi+c1+1]);
                giel=giel+1;
            end
        end
        i1=i1+1;
    end
    tt=tri2;
end

function [data, EE] = EdgeCutPoints(P, cutFaces, phi, tol, iel, Elements, tet2)
    global gc1 hPatchFace H
    
    edgesl = [1,2;2,3;1,3];
    EE = zeros(3*size(cutFaces,1),2);
    lo = 1;
    for i = 1:size(cutFaces,1)
        ifa = cutFaces(i,1:3);
        up = lo+2;
        EE(lo:up,:) = ifa([1:2;2:3;[3,1]]);
        lo = up+1;
    end
    EE = sort(EE,2); %Edge connectivity for all faces
    
%     iv = tet2(iel,:);
%     [3 19 18 6 87 88 85 86 89 70]
%     [19 18 22 6 88 75 90 89 70 74]
    
    data = NaN(4*3,4);
    ifaceInd = 1;
    lo = 1;
    %% Find Cut Points along element edges
    for iface = cutFaces'
        
%         iface = [19;6;18;89;70;88];
%         iface = [18;19;6;88;89;70];       
       
        x6 = P(iface,1); y6 = P(iface,2); z6 = P(iface,3); 
        X6 = [x6,y6,z6];
        PHI = phi(iface);
%         edges = iface(edgesl);
        cutEdgesInd = find(sum(PHI(edgesl)>=0,2) > 0 & sum(PHI(edgesl)>=0,2) < 2);
%         cutEdgesInds = edgesl(sum(PHI(edgesl)>=0,2) > 0 & sum(PHI(edgesl)>=0,2) < 2,:);
        
        cutEdgesIndG = (ifaceInd-1)*3+cutEdgesInd;
        
        % Get edge cut points
%         rc = InterpolateCutPoint(PHI);
        rc = FindEdgePoints(PHI,cutEdgesInd,tol);
        rc = rc(cutEdgesInd,:);
        
        nrc = size(rc,1);
        if nrc ~= 2
            error('Should be two points!');
        end
        
        %mid point
        rcm = mean(rc,1);
        r0 = rcm; %starting point
        fi = basetriP2.fi(r0);
        Pri = [fi*x6,fi*y6,fi*z6];
        
        % Get Normal. aka search direction
        % Note, this is critical for the convergence of the following
        % Newton method! Gradient direction gives optimal convergence.
        dfi0 = basetriP2.dfi(r0);
        dPHI0 = dfi0.'*PHI;
        n = dPHI0/norm(dPHI0);
        
%         dxdrs = [dfi'*x6, dfi'*y6, dfi'*z6];
%         nrs = cross(dxdrs(1,:),dxdrs(2,:));
%         nrs = nx/norm(nx);
%         J = [dxdrs;nrs]
        
%         dfidxyz = J\[dfidxsi;dfideta;dfidzeta]
        
%         fi = basetriP2.fi(n(:)');
%         nx = [fi*x6,fi*y6,fi*z6];
%         nx = nx/norm(nx);
%         quiver3(Pri(:,1),Pri(:,2),Pri(:,3),nx(1),nx(2),nx(3),1,'Color','r')
%         
        % Newtons method
        ri = FindSurfacePoint(r0,PHI,n,tol);
        
        %% Draw parametric triangle
%         xfigure();
%         patch(rs(:,1),rs(:,2),'w'); axis equal off tight; hold on; view(2)
%         text(rs(:,1),rs(:,2),strread(num2str(1:3),'%s'),'BackgroundColor','w')
%         text(rs(:,1),rs(:,2)+[-.1;-.1;.1]*1,cellstr(num2str(sign(PHI(1:3)))),'BackgroundColor','w')
% %         text(mean(rr(cutEdges),2),mean(ss(cutEdges),2), strread(num2str(cutEdgesIndG'),'%s'), 'BackgroundColor','y')
%         plot(rc(:,1),rc(:,2),'b-x')
%         quiver(rcm(1),rcm(2),n(1),n(2),0.4)
%         plot(ri(1),ri(2),'xk')
        
        %% Interpolate onto 3D triangle
        Pr = [rc;ri];
        fi = basetriP2.fi(Pr);
        PX = [fi*x6,fi*y6,fi*z6];
        
        %% Draw triangles
%         xfigure(45);
%         hp = patch(x6(1:3),y6(1:3),z6(1:3),'c'); axis equal off tight; hold on;
%         hp.FaceColor = 'c';
%         hp.FaceAlpha = 0.3;
% %         text(x6(1:3),y6(1:3),z6(1:3),cellstr(num2str( iface(1:3) )),'BackgroundColor','w')
% %         plot3(PX(:,1),PX(:,2),PX(:,3),'xk')
        %% Draw tet face
%         xfigure(45);
%         gc1 = gc1 +1;
%         if gc1 == 1
%             H.hpf = patch(x6(1:3),y6(1:3),z6(1:3),'c'); axis equal off tight; hold on;
%             H.hpf.FaceColor = 'b';
%             H.hpf.FaceAlpha = 0.1;
            
%             H.hpp = text(x6,y6,z6, cellstr(num2str(iface)), 'BackgroundColor','w');
%         else
%             H.hpf.XData = x6(1:3); 
%             H.hpf.YData = y6(1:3); 
%             H.hpf.ZData = z6(1:3); 
            
%             H.hpp.delete;
%             H.hpp = text(x6,y6,z6, cellstr(num2str(iface)), 'BackgroundColor','w');
%         end
%         hp = patch(x6(1:3),y6(1:3),z6(1:3),'c');
%         hp.FaceColor = 'none';
        
%         
%         text(x6(1:3),y6(1:3),z6(1:3),cellstr(num2str( iface(1:3) )),'BackgroundColor','w')
%         text(mean(x6),mean(y6),mean(z6),num2str(ifaceInd),'BackgroundColor','g')
%         text(mean(x6(cutEdges),2),mean(y6(cutEdges),2),mean(z6(cutEdges),2), strread(num2str(cutEdgesIndG'),'%s'), 'BackgroundColor','y')
%         plot3(PX(:,1),PX(:,2),PX(:,3),'xb')
%         if iel==8
%             plot3(Pri(:,1),Pri(:,2),Pri(:,3),'*b')
%             1;
%         end
%         if iel==9
%             plot3(Pri(:,1),Pri(:,2),Pri(:,3),'*g')
%             1;
%         end
        
        %%

        
        up = lo+2;
        data(lo:up,:) = [ PX, [cutEdgesIndG;NaN]];
        lo = up+1;
        ifaceInd = ifaceInd+1;
    end
end

function rP = FindRootAlongDir(ip,P,tet2,phi,th,iel)
    iv = tet2(iel,:);
    PHI = phi(iv);
    
    itet = tetP2(tet2,P,iel);
    fi0 = itet.fi(ip);
    dfi0 = itet.dfi(ip);
    dPHI0 = (dfi0.'*PHI).';
    PHI0 = fi0.'*PHI;
    
%     xfigure(56)
%     hp1 = plot(1,PHI0,'-ko');
%     f = NaN(40,1);
    
    searchDir = dPHI0;
    searchDir = searchDir/norm(searchDir);
    
    %% Compute optimal step length sl
    DD = -(PHI0./dPHI0*searchDir.')*searchDir; % Descent direction
%     tic
    np = 20;
    a = linspace(0,1,np)';
    h = repmat(ip,np,1)+repmat(a,1,3).*DD;
    fih = itet.fi(h);
    PHIih = fih.'*PHI;
    [~,ind] = min(abs(PHIih));
    sl = a(ind);
%     toc
    
    %%
    PHIi = PHI0;
    dPHIi = dPHI0;
    cc = 1;
    while abs(PHIi) > th        
        %% Newton
        DD = -(PHIi./dPHIi*searchDir.')*searchDir; % Descent direction
        ip = ip + DD*sl;
        fi = itet.fi(ip);
        dfi = itet.dfi(ip);
        PHIi = fi.'*PHI;
        dPHIi = (dfi.'*PHI).';
        
       
        
        %% Viz
%         xfigure(45)
%         plot3(ip(1),ip(2),ip(3),'ko','MarkerFaceColor','k')
%         
%         f(cc) = PHIi;        
%         hp1.XData = (1:cc).';
%         hp1.YData = f(~isnan(f));
%         drawnow
        %% Counter
        cc = cc +1;        
    end
    
%     xfigure(56)
%     hp1 = plot((1:cc-1),f(~isnan(f)),'-ko');
%     figure(56)
%     title('Root finding - Newton')
%     xlabel('Iteration')
%     ylabel('\phi')
%     xfigure(45)
%     plot3(ip(1),ip(2),ip(3),'ko','MarkerFaceColor','k')
    
    rP = ip;
%     1;
end


function rP = FindRootOnLine(ip,linePoints,P,tet2,phi,tol,th,iel)
    lp1 = linePoints(1,:); lp2 = linePoints(2,:);
    iv = tet2(iel,:);
    iX = P(iv,:); xc = iX(:,1); yc = iX(:,2); zc = iX(:,3);
    
    
    PHI = phi(iv);
    
    pa = lp1; pb = lp2;
    
    %% Sort pa and pb in acending order
    Pab = [pa;pb];
    [fiab,~,~,~,~] = basetetP2(Pab,xc,yc,zc);
    PHIab = fiab.'*PHI;
    [~,ai] = sort(PHIab);
    pa = Pab(ai(1),:); pb = Pab(ai(2),:);
    
    %% used in the Newton method
    searchDir = pb-pa; %ab
    searchDir = searchDir/norm(searchDir);
    sl = 1;
    
    
    [fi,fix,fiy,fiz,~] = basetetP2(ip,xc,yc,zc);
%     [fi,fix,fiy,fiz,~]=basetetsq(ip(1),ip(2),ip(3),xc,yc,zc);
    PHIi = fi.'*PHI;
    
    xfigure(56)
    hp1 = plot(1,PHIi,'-ko');
    f = NaN(40,1);
%     f(1) = PHIi;
%     xfigure(45)
%     hpa = plot3(pa(1),pa(2),pa(3),'ro','MarkerFaceColor','b');
% %     hta = text(pa(1),pa(2),pa(3),num2str(PHIab(ai(1))),'BackgroundColor','y');
%     hpb = plot3(pb(1),pb(2),pb(3),'bo','MarkerFaceColor','r');
% %     htb = text(pb(1),pb(2),pb(3),num2str(PHIab(ai(2))),'BackgroundColor','y');
    
    dPHIi = [fix.'*PHI, fiy.'*PHI, fiz.'*PHI];
    DD = -(PHIi./dPHIi*searchDir.')*searchDir;
    
    np = 20;
    a = linspace(0,1,np)';
    h = repmat(ip,np,1)+repmat(a,1,3).*DD;
    [fih,~,~,~,~] = basetetP2(h,xc,yc,zc);
    PHIih = fih.'*PHI;
    [~,ind] = min(abs(PHIih));
    sl = a(ind);

    cc = 1;
    while abs(PHIi) > tol
        %% Bisection
%         ip = (pa+pb)/2;
% %         xfigure(45)
% %         plot3(ip(1),ip(2),ip(3),'ko','MarkerFaceColor','k')
        
%         [fi,fix,fiy,fiz,~] = basetetP2(ip(1),ip(2),ip(3),xc,yc,zc); %Todo: Extract definition of fi if not using Newton method
% %         [fi,fix,fiy,fiz,~]=basetetsq(ip(1),ip(2),ip(3),xc,yc,zc);
%         PHIi = fi.'*PHI;
%         if PHIi < 0
%             pa = ip;
% %             hpa.XData = pa(1); hpa.YData = pa(2); hpa.ZData = pa(3);
%         else
%             pb = ip;
% %             hpb.XData = pb(1); hpb.YData = pb(2); hpb.ZData = pb(3);
%         end
        
        %% Newton
        dPHIi = [fix.'*PHI, fiy.'*PHI, fiz.'*PHI];
        DD = -(PHIi./dPHIi*searchDir.')*searchDir;
        ip = ip + DD*sl;
        [fi,fix,fiy,fiz,~] = basetetP2(ip,xc,yc,zc);
        PHIi = fi.'*PHI;
        
        xfigure(45)
        plot3(ip(1),ip(2),ip(3),'ko','MarkerFaceColor','k')
        
        %% Mixed
%         dPHIi = [fix.'*PHI, fiy.'*PHI, fiz.'*PHI];
%         DD = -(PHIi./dPHIi*searchDir.')*searchDir;
%         ip = ip + DD*sl;
%         
%         if isInsideInterval(linePoints,ip,th)
%             [fi,fix,fiy,fiz,~] = basetetP2(ip,xc,yc,zc);
%             PHIi = fi.'*PHI;
% %             sl = sl*1.05;
%         else
%             ip = (pa+pb)/2;
%             [fi,fix,fiy,fiz,~] = basetetP2(ip,xc,yc,zc);
%             PHIi = fi.'*PHI;
%             if PHIi < 0
%                 pa = ip;
%             else
%                 pb = ip;
%             end
%             sl = sl/2
%         end
        
        %% Viz
       

        f(cc) = PHIi;        
        hp1.XData = (1:cc).';
        hp1.YData = f(~isnan(f));
        drawnow
        %% Counter
        cc = cc +1;
%         if cc > 40
% %             rP = NaN(1,3);
%             break
%         end
        
%         v = ip-linePoints(1,:);
%         w = ip-linePoints(2,:);
%         if v*v' < th
%             break;
%         end
%         if w*w' < th
%             break;
%         end            
    end
    if isInsideInterval(linePoints,ip,th)
        rP = ip;
    else
        rP = NaN(1,3);
    end
    
%     xfigure(56)
%     hp1 = plot((1:cc-1),f(~isnan(f)),'-ko');
    figure(56)
    title('Root finding - Newton')
    xlabel('Iteration')
    ylabel('\phi')
    xfigure(45)
    plot3(ip(1),ip(2),ip(3),'ko','MarkerFaceColor','k')
    1;
end

function intersectPoints = LineTrianglesIntersection(ip,tetNormal,elesize,th,triangles,P,tet2,iel,phi)

    %% Gradient
    iv = tet2(iel,:);
    iX = P(iv,:); xc = iX(:,1); yc = iX(:,2); zc = iX(:,3);
    PHI = phi(iv);
    [fi,fix,fiy,fiz,~] = basetetP2(ip,xc,yc,zc);
%     [fi,fix,fiy,fiz,~]=basetetsq(ip(1),ip(2),ip(3),xc,yc,zc);
    dPHI = [fix'*PHI, fiy'*PHI, fiz'*PHI];
    dPHI = dPHI/norm(dPHI);
    
    %%
    lp1 = ip-dPHI*elesize*2;
    lp2 = ip+dPHI*elesize*2;
    %     plot3([lp1(1),lp2(1)],[lp1(2),lp2(2)],[lp1(3),lp2(3)],'kx-')
    nTri = size(triangles,1);
    intersectPoints = NaN(nTri,3);
    c = 0;
    for i = 1:nTri
        itri = triangles(i,:);
        tX = P(itri,:);
        v1 = tX(1,:); v2 = tX(2,:); v3 = tX(3,:);
        nt = cross(v2-v1,v3-v1);
        nt = nt/norm(nt);
        rI = (nt*(v1-lp1).')/(nt*(lp2-lp1).');
        if ~EdgeIsCut(rI); continue; end
        lpI = lp1 + rI*(lp2-lp1);

        u = v2-v1;
        v = v3-v1;
        w = lpI-v1;

        uv = (u*v.'); uw = (u*w.'); vw = (v*w.');
        uu = (u*u.'); vv = (v*v.');
        denom = uv^2-uu*vv;

        sI = (uv*vw-vv*uw) / denom;
        tI = (uv*uw-uu*vw) / denom;
        % Needs rounding because of numerical error
        isInsideFace = sI > 0-th && tI > 0-th && sI+tI < 1+th;
        if isInsideFace
            c = c+1;
            intersectPoints(c,:) = lpI;
        end
    end
    intersectPoints = intersectPoints(~all(isnan(intersectPoints),2),:);
end

function tetNormal = TetElementNormal(faces3,phi,xnod,ynod,znod)
    edges = [faces3(:,[1:2]);faces3(:,[2:3]);faces3(:,[3,1])];
    edges = unique(sort(edges,2),'rows');
    cutEdgesInds = sum(phi(edges) > 0,2) == 1;
    cutEdges = edges(cutEdgesInds,:);
    u = phi(cutEdges);
    [~,k]=sort(u,2);
    cutEdgesS = cutEdges;
    for i=1:size(cutEdgesS,1)
        cutEdgesS(i,:) = cutEdges(i,k(i,:));
    end
    nPHIv = [xnod(cutEdgesS(:,2))-xnod(cutEdgesS(:,1)),...
         ynod(cutEdgesS(:,2))-ynod(cutEdgesS(:,1)),...
         znod(cutEdgesS(:,2))-znod(cutEdgesS(:,1))];
    tetNormal = mean(nPHIv,1); % Normal
    tetNormal = tetNormal/norm(tetNormal);

end

function [nodes,PX] = MergeAndOrientDataPoints(data, EE)
    data = data(~all(isnan(data),2),:);
    PX = data(:,[1:3]);
    
%     xfigure(45);
%     plot3(PX(:,1),PX(:,2),PX(:,3),'rx')
    
    pinds = ~isnan(data(:,end));
    einds = data(pinds,end);
    nP = size(einds,1);
    edges = [EE(einds,:),zeros(nP,1)];
    edges(1:2:nP,3)=1:nP/2;
    edges(2:2:nP,3)=1:nP/2;
    %%
    ind0 = zeros(nP/2,1); ind0(1:2) = 1:2;
    ind1 = zeros(nP/2,1); ind1(1) = 1;
    ed = edges(2,1:2);
    iv = zeros(nP/2,1); iv(1) = 2;
    
    for i = 2:nP/2-1
        ia = find(all(ismember(edges(:,1:2),ed),2));
        ia = setdiff(ia,iv);
        if length(ia) > 1; ia = ia(1); end; 
        ind1(i) = edges(ia,3);
        if mod(ia,2) == 0; s = -1; else; s = 1; end
        ed = edges(ia+s,1:2);
        iv(i) = ia+s;
        ind0(i+1) = ia+s;
    end
    ind1(end) = setdiff(1:length(ind1),ind1);
    
    
    %%
    PX0 = PX(pinds,:);
    PX0 = PX0(ind0,:);
    PX1 = PX(~pinds,1:3);
    PX1 = PX1(ind1,:);
    %%
    nodes = [1:length(ind0)+length(ind1)];
    PX = [PX0;PX1];
end

function n = SearchDirection(ri,PHI)
    dfi = basetriP2.dfi(ri);
    dPHI = dfi.'*PHI;
    n = dPHI/norm(dPHI);
    
%     nef = zeros(2,2);
%     i = 1;
%     for iedge = cutEdges'
%         q  = PHI(iedge);
%         [u,k] = sort(q);
%         Xi = rs(iedge(k),:);
%         nef(i,:) = Xi(2,:)-Xi(1,:);
%         i = i+1;
%     end
%     n = mean(nef,1);
%     n = n/norm(n);
end

function ri = FindSurfacePoint(ri,PHI,n,tol)
    [fi,dfidr,dfids] = baseP2(ri);
    PHIi = fi*PHI;
    
%     xfigure(56)
%     hp1 = plot(1,PHIi,'-ko');
%     title('Root finding - Newton')
%     xlabel('Iteration')
%     ylabel('\phi')
%     f = NaN(40,1);
%     f(1) = PHIi;
%     cc = 2;
    while abs(PHIi) > tol
        dPHIi = [dfidr*PHI,dfids*PHI];
        ri = ri - (PHIi/(dPHIi*n(:)))*n(:).';
        [fi,dfidr,dfids] = baseP2(ri);
        PHIi = fi*PHI;
        
%         f(cc) = PHIi;
%         hp1.XData = (1:cc);
%         hp1.YData = f(~isnan(f)).';
%         drawnow
%         cc = cc +1; 
    end
%     xfigure(56)
%     hp1 = plot((1:cc-1),f(~isnan(f)),'-ko');
    
    %% If the Point is outside of the triangle
    if ~isInsideTri(ri)
%         error('Extensive curvature detected. Refine the mesh!')
        warning('Extensive curvature detected. Surface is reaching outside of current element.')
    end
    
end

function iit = isInsideTriangle(ri)
    p1x = 0; p1y = 0;
    p2x = 1; p2y = 0;
    p3x = 0; p3y = 1;
    px = ri(1); py = ri(2);
    s = (p1y*p3x - p1x*p3y + (p3y - p1y)*px + (p1x - p3x)*py);
    t = (p1x*p2y - p1y*p2x + (p1y - p2y)*px + (p2x - p1x)*py);
    
    iit = s >= 0 && t >= 0 && (1-s-t) >= 0;
    
end

function rc = InterpolateCutPoint(PHI)
    rs = [0,0;1,0;0,1;0.5,0;0.5,0.5;0,0.5]; 
%     xfigure();
%     patch(rs(1:3,1),rs(1:3,2),'w'); hold on
%     text(rs(:,1),rs(:,2),cellstr(num2str( (1:6).' )),'BackgroundColor','w')
%     text(rs(:,1),rs(:,2),cellstr(num2str( PHI )),'BackgroundColor','w')
    
    edgesl = [1,4;4,2;2,5;5,3;3,6;6,1];
    cutEdgesInd = sum(PHI(edgesl)>=0,2) > 0 & sum(PHI(edgesl)>=0,2) < 2;
    cutEdges = edgesl(cutEdgesInd,:);
    ncuts = size(cutEdges,1);
    rc = zeros(ncuts,2);
    for ied = 1:ncuts
        [u,k] = sort(PHI(cutEdges(ied,:)));
        ed = cutEdges(ied,:);
        rsi = rs( ed(k), : );
        % Linear interpolation
        rc(ied,:) = rsi(1,:) + (rsi(2,:)-rsi(1,:))*((0-u(1))/(u(2)-u(1)));
    end
%     plot(rc(:,1),rc(:,2),'ko')
end

function rc = FindEdgePoints(PHI,cutEdgesInd,tol)
    rc = NaN(3,2);
    
    if any(cutEdgesInd == 1)
        edge = [1,2];
        rc(1,:) = FindRootOnEdge(edge,PHI,tol);
    end
    if any(cutEdgesInd == 2)
        edge = [2,3];
        rc(2,:) = FindRootOnEdge(edge,PHI,tol);
    end
    if any(cutEdgesInd == 3)
        edge = [3,1];
        rc(3,:) = FindRootOnEdge(edge,PHI,tol);
    end
end

function ri = FindRootOnEdge(edge,PHI,tol)
    rs = [0,0;1,0;0,1];
    rse = rs(edge,:);
    searchDir = rse(2,:)-rse(1,:);
    searchDir = searchDir/norm(searchDir);
    
    ri = rse(1,:);
    [~,ind] = sort(PHI(edge));
    ra = rse(ind(1),:); rb = rse(ind(2),:);
    [fi,dfidr,dfids] = baseP2(ri);
    PHIi = fi*PHI;
%     sprintf('Iteration | PHIi | ri |')
%     c = 1;
    while abs(PHIi) > tol
        dPHIi = [dfidr*PHI,dfids*PHI];
        ri = ri - (PHIi/(dPHIi*searchDir'))*searchDir;
        
        if ~isInsideTri(ri) % Bisect
            ri = (ra+rb)/2;
            [fi,dfidr,dfids] = baseP2(ri);
            PHIi = fi*PHI;
            if PHIi < 0
                ra = ri;
            else
                rb = ri;
            end
        else
            [fi,dfidr,dfids] = baseP2(ri);
            PHIi = fi*PHI;
        end
%         [fi,dfidr,dfids] = baseP2(ri);
%         PHIi = fi*PHI;
        
%         fprintf('%d | %9.6f | %9.6f |  %9.6f|\n',c,PHIi,ri(1), ri(2))
%         c = c+1;
    end
%     1;
end

function ret = isInsideInterval(linePoints,ip,epsilon)
    lp1 = linePoints(1,:); lp2 = linePoints(2,:);
    
    u = (lp2-lp1)*(ip-lp1).';
    v = (lp2-lp1)*(lp2-lp1).';
    
    ret = u > 0-epsilon && u < v+epsilon;
    
end

function iscut = EdgeIsCut(rI)
    if isnan(rI)
        % (n*(P1-P0).' = 0 which means line is parallel to plane. No
        % intersection of plane
        iscut = 0;
    elseif rI >= 0 && rI <= 1
        %We have intersection of the plane.
        %                rI
        iscut = 1;
    else
        % No intersection.
        iscut = 0;
    end
end

function test = isInsideTri(ri)
    tol = eps*10;
    test = ri(1)>0-tol & ri(1)<1+tol &...
           ri(2)>0-tol & ri(2)<1+tol &...
           1-ri(1)-ri(2) > 0-tol;
end

function rs = quadInterpolate(PHI,cutEdgesInd)
    u1 = PHI(1); u2 = PHI(2); u3 = PHI(3);
    u4 = PHI(4); u5 = PHI(5); u6 = PHI(6);
    
    %a*x^2+b*x+c=0
    
    syms r s u1 u2 u3 u4 u5 u6 
    u = [u1,u2,u3,u4,u5,u6].';
    fi = [(1-r-s).*(1-2*r-2*s),r.*(2*r-1),s.*(2*s-1),4*r.*(1-r-s),4*r.*s,4*s.*(1-r-s)]
    edge = [2,5,3]
    f = subs(fi(edge)*u(edge),r,1-s)
    f = collect(f,'s')
    
    if any(cutEdgesInd == 3)
        %Edge 1,6,3
        r3 = 0;
        a = (2*u2 + 2*u3 - 4*u5); b = (-3*u2 - u3 + 4*u5); c = u2;
        
        sol = [(-b + sqrt(b^2 - 4*a*c))/(2*a)
               (-b - sqrt(b^2 - 4*a*c))/(2*a)]
        
    else
        r3 = NaN; s3 = NaN;
    end
        
    if any(cutEdgesInd == 1)
        %Edge 1,4,2
        s1 = 0;
        a = u1; b = 2*u1 + 2*u2 - 4*u4; c = -3*u1 - u2 + 4*u4;
        if b == 0
            r1 = -a/c;
        elseif c == 0
            r1 = NaN;
        else
            r1 = -(c + (c^2 - 4*a*b)^(1/2))/(2*b);
            if r1 < 0 || r1 > 1
                 r1 = -(c - (c^2 - 4*a*b)^(1/2))/(2*b);
                 if r1 < 0 || r1 > 1
                     r1 = NaN;
                 end
            end
        end
        if ~isreal(r1)
            r1 = NaN;
        end
        if isnan(r1) && c ~= 0
            % Switch to linear interpolation
            q = [u1,u2];
            %             [u,k] = sort(q);
            t = (0-q(1))/(q(2)-q(1));
            %             ri = [1,0]+([0,1]-[1,0])*t
            r1 = t;
            s1 = 0;
        end
    else
        r1 = NaN; s1 = NaN;
    end
    
    if any(cutEdgesInd == 2)
        %Edge 2,5,3
        a = u1; b = 2*u2 + 2*u3 - 4*u5; c = -u2 - 3*u3 + 4*u5;
        if b == 0
            r2 = NaN;
        elseif c == 0
            r2 = (a)^(1/2)/b^(1/2);
        else
            r2 = -(c + (c^2 - 4*a*b)^(1/2))/(2*b);
            if r2 < 0 || r2 > 1
                 r2 = -(c - (c^2 - 4*a*b)^(1/2))/(2*b);
                 if r2 < 0 || r2 > 1
                     r2 = NaN;
                 end
            end
        end
        s2 = 1-r2;
        if ~isreal(r2)
            r2 = NaN;
            s2 = NaN;
        end
        if isnan(r2) && c ~= 0
            % Switch to linear interpolation
            q = [u2,u3];
%             [u,k] = sort(q);
            t = (0-q(1))/(q(2)-q(1));
%             ri = [1,0]+([0,1]-[1,0])*t
            r2 = 1-t;
            s2 = t;
        end
    else
        r2 = NaN; s2 = NaN;
    end
    
    rs = [r1,s1;r2,s2;r3,s3];
    
end





%% Functions

function fi = baseP2v(rs)
    r = rs(:,1);
    s = rs(:,2);
    
    
    fi = [(1-r-s).*(1-2*r-2*s),r.*(2*r-1),s.*(2*s-1),4*r.*(1-r-s),4*r.*s,4*s.*(1-r-s)];
%     dfidr = [4*s+4*r-3,4*r-1,0*s,-4*(s+2*r-1),4*s,-4*s];
%     dfids = [4*s+4*r-3,0*s,4*s-1,-4*r,4*r,-4*(2*s+r-1)];

end

function [fi,dfidr,dfids] = baseP2(rs)
    r = rs(1);
    s = rs(2);

    fi = [(1-r-s)*(1-2*r-2*s),r*(2*r-1),s*(2*s-1),4*r*(1-r-s),4*r*s,4*s*(1-r-s)];
    dfidr = [4*s+4*r-3,4*r-1,0,-4*(s+2*r-1),4*s,-4*s];
    dfids = [4*s+4*r-3,0,4*s-1,-4*r,4*r,-4*(2*s+r-1)];

end
function [fi,dfidr,dfids,n] = baseP2n(r,s,x6,y6,z6)

    fi = [(1-r-s)*(1-2*r-2*s),r*(2*r-1),s*(2*s-1),4*r*(1-r-s),4*r*s,4*s*(1-r-s)];
    dfidr = [4*s+4*r-3,4*r-1,0,-4*(s+2*r-1),4*s,-4*s];
    dfids = [4*s+4*r-3,0,4*s-1,-4*r,4*r,-4*(2*s+r-1)];

    dxdr = [dfidr*x6, dfidr*y6, dfidr*z6];
    dxds = [dfids*x6, dfids*y6, dfids*z6];
    n1 = cross(dxdr,dxds);
    n = n1/norm(n1);
end

function dir = UpDir(dir,n)
    for i = 1:n
        dir = fileparts(dir);
    end
end
function computingInterpolant
    %%
    syms r s u1 u2 u3 u4 u5 u6
    fi = [(1-r-s)*(1-2*r-2*s),r*(2*r-1),s*(2*s-1),4*r*(1-r-s),4*r*s,4*s*(1-r-s)];
    u = [u1,u2,u3,u4,u5,u6];
    
    ed = [1,6,3];
    ekv = subs(fi(ed)*u(ed).',r,0) == 0;
    s1 = solve(ekv,s)
%     s1 = s1(2)
    r1 = 0
    
    ed = [1,4,2];
    ekv = subs(fi(ed)*u(ed).',s,0) == 0;
    r2 = solve(ekv,r)
%     r2 = r2(2)
    s2 = 0
    
    ed = [2,5,3];
    ekv = subs(fi(ed)*u(ed).',s,1-r) == 0;
    r3 = solve(ekv,r)
%     r3 = r3(2)
    s3 = 1-r3
end

function [gcx,gcy,gv]=trigauc(xc,yc,poldegree)
% function [gc,gv]=trigauc(xc,yc,poldegree)
%
%     calculate coordinates and weight for Gauss quadrature
%      in the triangle with vertices in xc(*),yc(*)
%
%     poldegree: degree of polynomial to be integrated exactly (1,2,3,4,5,7,8 or 11)
%
switch(poldegree)
case 1
	gv=1;
	gcx=(xc(1)+xc(2)+xc(3))/3;
	gcy=(yc(1)+yc(2)+yc(3))/3;
case 2
	gv=[1/3,1/3,1/3];
	gcx=[(xc(1)+xc(2))/2,(xc(2)+xc(3))/2,(xc(3)+xc(1))/2];
	gcy=[(yc(1)+yc(2))/2,(yc(2)+yc(3))/2,(yc(3)+yc(1))/2];
case 3
	gv=[3,3,3,8,8,8,27]/60;
	gcx=[xc(1),xc(2),xc(3),(xc(1)+xc(2))/2,(xc(2)+xc(3))/2, ...
	(xc(3)+xc(1))/2,(xc(1)+xc(2)+xc(3))/3];
	gcy=[yc(1),yc(2),yc(3),(yc(1)+yc(2))/2,(yc(2)+yc(3))/2, ...
	(yc(3)+yc(1))/2,(yc(1)+yc(2)+yc(3))/3];
case 4
	gcx=zeros(1,6);gcy=gcx;gv=gcx;
	x1=0.8168475729804585;y1=0.09157621350977073;z1=1-x1-y1;  % multiplicity 3
	w1=0.3298552309659655/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(1:3)=xv;gcy(1:3)=yv;gv(1:3)=vv;	
	x1=0.1081030181680702;y1=0.4459484909159649;z2=1-x1-y1;  % multiplicity 3
 	w1=0.6701447690340345/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(4:6)=xv;gcy(4:6)=yv;gv(4:6)=vv;	
case 5
	x1=0.101286507323456;
	x2=0.797426985353087;
	x3=x1;
	x4=0.470142064105115;
	x5=x4;
	x6=0.059715871789770;
	x7=1/3;
	xsi=[x1,x2,x3,x4,x5,x6,x7];eta=[x1,x1,x2,x6,x4,x4,x7];
	w1=0.125939180544827;w4=0.132394152788506;w7=0.225;
	gv=[w1,w1,w1,w4,w4,w4,w7];
    gcx=[xsi*xc(1)+eta*xc(2)+(1-xsi-eta)*xc(3)];
    gcy=[xsi*yc(1)+eta*yc(2)+(1-xsi-eta)*yc(3)];
case 6
	gcx=zeros(1,12);gcy=gcx;gv=gcx;
	x1=0.5014265096581342;y1=0.2492867451709329;z1=1-x1-y1;  % multiplicity 3
	w1=0.3503588271790222/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(1:3)=xv;gcy(1:3)=yv;gv(1:3)=vv;	
	x2=0.8738219710169965;y2=0.06308901449150177;z2=1-x2-y2;  % multiplicity 3
 	w2=0.1525347191106164/3;
	[xv,yv,vv]=multiplicity(x2,y2,w2,xc,yc,3);
	gcx(4:6)=xv;gcy(4:6)=yv;gv(4:6)=vv;	
	x3=0.6365024991213939;y3=0.05314504984483216;z3=1-x3-y3;  % multiplicity 6
	w3=0.4971064537103575/6;
	[xv,yv,vv]=multiplicity(x3,y3,w3,xc,yc,6);
	gcx(7:12)=xv;gcy(7:12)=yv;gv(7:12)=vv;	
case 7
	x1=0.065130102902216;
	x2=0.869739794195568;
	x3=x1;
	x4=0.312865496004874;
	x5=0.638444188569810;
	x6=0.048690315425316;
	x7=x5;
	x8=x4;
	x9=x6;
	x10=0.260345966079040;
	x11=0.479308067841920;
	x12=x10;
	x13=1/3;
	xsi=[x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13];
	eta=[x1,x1,x2,x6,x4,x5,x6,x5,x4,x10,x10,x11,x13];
	w1=0.053347235608838;w4=0.077113760890257;w10=0.175615257433208;
	w13=-0.149570044467682;
	gv=[w1,w1,w1,w4,w4,w4,w4,w4,w4,w10,w10,w10,w13];
    gcx=[xsi*xc(1)+eta*xc(2)+(1-xsi-eta)*xc(3)];
    gcy=[xsi*yc(1)+eta*yc(2)+(1-xsi-eta)*yc(3)];
case 8
	gcx=zeros(1,16);gcy=gcx;gv=gcx;
	x1=1/3;y1=1/3;z1=1-x1-y1;  % multiplicity 1
	w1=0.1443156076777862;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,1);
	gcx(1)=xv;gcy(1)=yv;gv(1)=vv;	

	x1=0.08141482341455413;y1=0.4592925882927229;z1=1-x1-y1;  % multiplicity 3
	w1=0.2852749028018549/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(2:4)=xv;gcy(2:4)=yv;gv(2:4)=vv;	
	
	x1=0.8989055433659379;y1=0.05054722831703103;z1=1-x1-y1;  % multiplicity 3
 	w1=0.09737549286959440/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(5:7)=xv;gcy(5:7)=yv;gv(5:7)=vv;	
	
	x1=0.6588613844964797;y1=0.1705693077517601;z1=1-x1-y1;  % multiplicity 3
 	w1=0.3096521116041552/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(8:10)=xv;gcy(8:10)=yv;gv(8:10)=vv;	
	
	x1=0.0083947774099572110;y1=0.7284923929554041;z1=1-x1-y1;  % multiplicity 6
	w1=0.1633818850466092/6;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,6);
	gcx(11:16)=xv;gcy(11:16)=yv;gv(11:16)=vv;	
case 11
	gcx=zeros(1,28);gcy=gcx;gv=gcx;
	x1=0.9480217181434233;y1=0.02598914092828833;z1=1-x1-y1;  % multiplicity 3
	w1=0.02623293466120857/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(1:3)=xv;gcy(1:3)=yv;gv(1:3)=vv;	
	
	x1=0.8114249947041546;y1=0.09428750264792270;z1=1-x1-y1;  % multiplicity 3
 	w1=0.1142447159818060/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(4:6)=xv;gcy(4:6)=yv;gv(4:6)=vv;	
	
	x1=0.01072644996557060;y1=0.4946367750172147;z1=1-x1-y1;  % multiplicity 3
	w1=0.05656634416839376/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(7:9)=xv;gcy(7:9)=yv;gv(7:9)=vv;	
	
	x1=0.5853132347709715;y1=0.2073433826145142;z1=1-x1-y1;  % multiplicity 3
 	w1=0.2164790926342230/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(10:12)=xv;gcy(10:12)=yv;gv(10:12)=vv;	
	
	x1=0.1221843885990187;y1=0.4389078057004907;z1=1-x1-y1;  % multiplicity 3
 	w1=0.20798741611661160/3;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
	gcx(13:15)=xv;gcy(13:15)=yv;gv(13:15)=vv;	
	
	x1=0;y1=0.858870281282263640;z1=1-x1-y1;  % multiplicity 6
	w1=0.04417430269980344/6;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,6);
	gcx(16:21)=xv;gcy(16:21)=yv;gv(16:21)=vv;	

	x1=0.04484167758913055;y1=0.6779376548825902;z1=1-x1-y1;  % multiplicity 6
	w1=0.2463378925757316/6;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,6);
	gcx(22:27)=xv;gcy(22:27)=yv;gv(22:27)=vv;	

	x1=1/3;y1=1/3;z1=1-x1-y1;  % multiplicity 1
	w1=0.08797730116222190;
	[xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,1);
	gcx(end)=xv;gcy(end)=yv;gv(end)=vv;	

otherwise 
	disp('Polynomial degrees are 1,2,3,4,5,7,8,11 in trigauc')
	gcx=[];gcy=[];gv=[];
end
end
function [xv,yv,vv]=multiplicity(x,y,v,xc,yc,m)
    z=[x,y,1-x-y];
    switch(m)
    case 1
        xv=[xc(1)*z(1)+xc(2)*z(2)+xc(3)*z(3)];
        yv=[yc(1)*z(1)+yc(2)*z(2)+yc(3)*z(3)];
        vv=v;
    case 3
        xv=[xc(1)*z(1)+xc(2)*z(2)+xc(3)*z(3),xc(1)*z(2)+xc(2)*z(3)+xc(3)*z(1),...
        xc(1)*z(3)+xc(2)*z(1)+xc(3)*z(2)];
        yv=[yc(1)*z(1)+yc(2)*z(2)+yc(3)*z(3),yc(1)*z(2)+yc(2)*z(3)+yc(3)*z(1),...
        yc(1)*z(3)+yc(2)*z(1)+yc(3)*z(2)];
        vv=v*ones(1,3);
    case 6
        xv=[xc(1)*z(1)+xc(2)*z(2)+xc(3)*z(3),xc(1)*z(1)+xc(2)*z(3)+xc(3)*z(2),...
        xc(1)*z(2)+xc(2)*z(1)+xc(3)*z(3),xc(1)*z(2)+xc(2)*z(3)+xc(3)*z(1),...
        xc(1)*z(3)+xc(2)*z(1)+xc(3)*z(2),xc(1)*z(3)+xc(2)*z(2)+xc(3)*z(1)];
        yv=[yc(1)*z(1)+yc(2)*z(2)+yc(3)*z(3),yc(1)*z(1)+yc(2)*z(3)+yc(3)*z(2),...
        yc(1)*z(2)+yc(2)*z(1)+yc(3)*z(3),yc(1)*z(2)+yc(2)*z(3)+yc(3)*z(1),...
        yc(1)*z(3)+yc(2)*z(1)+yc(3)*z(2),yc(1)*z(3)+yc(2)*z(2)+yc(3)*z(1)];
        vv=v*ones(1,6);
    end
end