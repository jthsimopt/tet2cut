function [ GP, GW ] = QuadIntegration(order, domain)
%QuadIntegration Produces Gauss integration points for quadliteral elements
%   [ GP, GW ] = QuadIntegration(order, domain)
%   order is Gauss Integration order
%   domain can either be [0,1] or [-1,1]
    
    if isequal(domain,[-1,1])
        [r, w, ~] = gauss(order,-1,1);
    elseif isequal(domain,[0,1])
        [r, w, ~] = gauss(order,0,1);
    else
       error('Domain can either be [0,1] or [-1,1]') 
    end
    [gx,gy] = meshgrid(r,r);
    [gwx,gwy] = meshgrid(w,w);
    GP = [gx(:),gy(:)];
    GW = [gwx(:).*gwy(:)];

end

function [x, w, A] = gauss(n, a, b)
    % 3-term recurrence coefficients:
    n = 1:(n - 1);
    beta = 1 ./ sqrt(4 - 1 ./ (n .* n));
    % Jacobi matrix:
    J = diag(beta, 1) + diag(beta, -1);
    % Eigenvalue decomposition:
    % e-values are used for abscissas, whereas e-vectors determine weights.
    [V, D] = eig(J);
    x = diag(D);
    % Size of domain:
    A = b - a;
    % Change of interval:
    %
    % Initally we have I0 = [ -1, 1 ]; now one gets I = [ a, b ] instead.
    %
    % The abscissas are Legendre points.
    %
    if ~(a == -1 && b == 1)
        x = 0.5 * A * x + 0.5 * (b + a);
    end
    % Weigths:
    w = V(1, :) .* V(1, :);
end