function [fi,fix,fiy,fiz,vol]=basetetsq(x,y,z,xc,yc,zc)
% Scalar evaluation of basis functions
A=[1,xc(1),yc(1),zc(1),xc(1)*yc(1),xc(1)*zc(1),zc(1)*yc(1),xc(1)*xc(1),yc(1)*yc(1),zc(1)*zc(1);
    1,xc(2),yc(2),zc(2),xc(2)*yc(2),xc(2)*zc(2),zc(2)*yc(2),xc(2)*xc(2),yc(2)*yc(2),zc(2)*zc(2);
    1,xc(3),yc(3),zc(3),xc(3)*yc(3),xc(3)*zc(3),zc(3)*yc(3),xc(3)*xc(3),yc(3)*yc(3),zc(3)*zc(3);
    1,xc(4),yc(4),zc(4),xc(4)*yc(4),xc(4)*zc(4),zc(4)*yc(4),xc(4)*xc(4),yc(4)*yc(4),zc(4)*zc(4);
    1,xc(5),yc(5),zc(5),xc(5)*yc(5),xc(5)*zc(5),zc(5)*yc(5),xc(5)*xc(5),yc(5)*yc(5),zc(5)*zc(5);
    1,xc(6),yc(6),zc(6),xc(6)*yc(6),xc(6)*zc(6),zc(6)*yc(6),xc(6)*xc(6),yc(6)*yc(6),zc(6)*zc(6);
    1,xc(7),yc(7),zc(7),xc(7)*yc(7),xc(7)*zc(7),zc(7)*yc(7),xc(7)*xc(7),yc(7)*yc(7),zc(7)*zc(7);
    1,xc(8),yc(8),zc(8),xc(8)*yc(8),xc(8)*zc(8),zc(8)*yc(8),xc(8)*xc(8),yc(8)*yc(8),zc(8)*zc(8);
    1,xc(9),yc(9),zc(9),xc(9)*yc(9),xc(9)*zc(9),zc(9)*yc(9),xc(9)*xc(9),yc(9)*yc(9),zc(9)*zc(9);
    1,xc(10),yc(10),zc(10),xc(10)*yc(10),xc(10)*zc(10),zc(10)*yc(10),xc(10)*xc(10),yc(10)*yc(10),zc(10)*zc(10)];
detj=det(A);
fim=A\eye(10);
fi=(fim'*[1;x;y;z;x*y;x*z;z*y;x*x;y*y;z*z]).';
fix=(fim'*[0;1;0;0;y;z;0;2*x;0;0]).';
fiy=(fim'*[0;0;1;0;x;0;z;0;2*y;0]).';
fiz=(fim'*[0;0;0;1;0;x;y;0;0;2*z]).';
fi=fi';fix=fix';fiy=fiy';fiz=fiz';

v=cross([xc(2)-xc(1),yc(2)-yc(1),zc(2)-zc(1)],[xc(3)-xc(1),yc(3)-yc(1),zc(3)-zc(1)]);
vol=abs(dot(v,[xc(4)-xc(1),yc(4)-yc(1),zc(4)-zc(1)]))/6;