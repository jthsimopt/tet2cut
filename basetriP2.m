classdef basetriP2 < handle
    %basetriP2 Summary of this class goes here
    %   Detailed explanation goes here
    
    methods (Static)
        function val = fi(ri)
            r = ri(:,1);
            s = ri(:,2);
            val = [(1-r-s).*(1-2*r-2*s),r.*(2*r-1),s.*(2*s-1),4*r.*(1-r-s),4*r.*s,4*s.*(1-r-s)];
        end
        
        function val = dfi(ri)
            r = ri(:,1);
            s = ri(:,2);
            dfidr = [4*s+4*r-3,4*r-1,0,-4*(s+2*r-1),4*s,-4*s];
            dfids = [4*s+4*r-3,0,4*s-1,-4*r,4*r,-4*(2*s+r-1)];
            val = [dfidr(:),dfids(:)];
        end
    end
    
end

