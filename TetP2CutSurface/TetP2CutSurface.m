function CutElements = TetP2CutSurface(tet,P,Elements,phi)
%     global H
    %% Parameters
    tol = 1e-9; %Netwon convergence tolerance
    
    %% Properties
%     nele = size(tet,1);

    % Find all surface function values that are exactly 0 and offset these
    % such that we avoid problems with zero surface values.
    phi0Map = phi == 0; 
    phi(phi0Map) = phi(phi0Map)+sqrt(eps) ;
    
    PHI = phi(tet);
    cutEle = find(min(PHI,[],2).* max(PHI,[],2)<0); %Efficient method of finding cut elements.
    
%     elesize = (max(P(:,1))-min(P(:,1)))/(nele/6)^(1/3);
%     th = sqrt(eps) * elesize;  
    
    %% Preallocate
    nCutEle = length(cutEle);
    CutElements(nCutEle) = Elements(1);
    
    %% Main Loop
    ice = 1;
    for iel = cutEle'
        faces = Elements(iel).faces;
        faces3 = faces(:,1:3);
        PHIF = phi(faces);
        cutFaces = faces(min(PHIF,[],2).*max(PHIF,[],2) < 0, :);
        
        %% Find Edge cut points on tet
        [data, EE] = SurfacePoints(P, cutFaces, phi, tol, iel, Elements, tet);
        
        %% Merge and orient the datapoints
        [tt,iP0] = MergeAndOrientDataPoints(data, EE);
        nP = length(tt);
        
        %% Tetnormal in order to orient face normal later
        tetNormal = TetElementNormal(faces3,phi,P);
        
        %% If Serendipity, create midpoint in order to split the serendipity into triangles
        if nP == 8
            iP = iP0;
            iele = Q8Element(iP);
            n = iele.BaseFunction.n;
            nm = mean(n,1);
            if nm*tetNormal' < 0
                %Flip
                tt = [1,4,3,2,8,7,6,5];
                iP = iP(tt,:);
                iele = Q8Element(iP);
            end
%             n = iele.BaseFunction.n;
            
            %% Save
            CutElements(ice).edges = Elements(iel).edges;
            CutElements(ice).faces = Elements(iel).faces;
            CutElements(ice).neighs = Elements(iel).neighs;
            CutElements(ice).iel = iel;
            CutElements(ice).surfTri = [1,2,3,4,5,6,7,8];
            CutElements(ice).surfType = 'Quad8';
            CutElements(ice).surfPoints = iP;
            CutElements(ice).surfElement = iele;
        else
            iP = iP0;
            
            iele = P2Element(iP,'gorder',2);
            n = iele.BaseFunction.n;
            nm = mean(n,1);
            if nm*tetNormal' < 0
                %Flip
                tt = [1,3,2,6,5,4];
                iP = iP(tt,:);
                iele = P2Element(iP);
            end
%             n = iele.BaseFunction.n;
            
            %% Save
            CutElements(ice).edges = Elements(iel).edges;
            CutElements(ice).faces = Elements(iel).faces;
            CutElements(ice).neighs = Elements(iel).neighs;
            CutElements(ice).iel = iel;
            CutElements(ice).surfTri = [1,2,3,4,5,6];
            CutElements(ice).surfType = 'Tri6';
            CutElements(ice).surfPoints = iP;
            CutElements(ice).surfElement = iele;
            
        end
        
        
        
        
        
        %% Viz
%         if H.c == 0
%             xfigure(3);
% %             H.hpf = patch('Faces',faces3,'Vertices',P,'FaceColor','b'); axis equal off tight; hold on;
% %             H.hpf.FaceAlpha = 0.1;
% %             H.hpf2 = patch('Faces',faces3,'Vertices',P,'FaceColor','none');
% %             VizIsoLines(tet2,P,iel,faces,phi)
%             H.c = H.c+1;
%             light
%             axis equal tight; hold on;
%         else
% %             H.hpf.Faces = faces3;
% %             H.hpf2 = patch('Faces',faces3,'Vertices',P,'FaceColor','none');
%             H.c = H.c+1;
%         end
%         if nP==8
%             iele.vizQ8Ele(10,'FaceColor','c', 'InnerEdgeColor', [0,1,1]*0.7,'InnerEdgeAlpha', 0.1);
%             axis equal tight; hold on;
%             plot3(iP(:,1),iP(:,2),iP(:,3),'ko','MarkerFaceColor','k')
%             GP = iele.GaussPoints;
%             quiver3(GP(:,1),GP(:,2),GP(:,3),n(:,1),n(:,2),n(:,3),0.3,'Color','b')
%         else
%             iele.VizP2Ele(10,'FaceColor','r', 'InnerEdgeColor', [1,0,0]*0.7, 'InnerEdgeAlpha', 0.1);
%             axis equal tight; hold on;
%             plot3(iP(:,1),iP(:,2),iP(:,3),'ko','MarkerFaceColor','k')
%             GP = iele.GaussPoints;
%             quiver3(GP(:,1),GP(:,2),GP(:,3),n(:,1),n(:,2),n(:,3),0.3,'Color','b')
%         end
%         drawnow
%         1;
        ice = ice + 1;
    end

end
function VizIsoLines(tet2,P,iel,faces,phi)
    %% Isolines
    itet = tetP2(tet2,P,iel);
    rs = [0,0;1,0;0,1;0.5,0;0.5,0.5;0,0.5];

    i = 1;
    iFv = faces(i,:);
    PHI= phi(iFv);
    tt = [1,4,6;6,5,3;4,5,6;4,2,5];
    tri = [1,2,3,4,5,6];
    %         [T,X,kk] = P2Interp(tri,[rs,ones(6,1)],4);
    [T,X,UI,kk] = P2Interp(tri,[rs,ones(6,1)*0],PHI,30);

    xfigure(55);
    hp = patch('Faces',T,'Vertices',X,'FaceColor','interp','CData',UI); hold on;
    hp.EdgeColor = 'none';
    E = sort([T(:,1) T(:,2); T(:,2) T(:,3); T(:,3) T(:,1)]')';
    [u,m,n] = unique(E,'rows');
    % determine uniqueness of edges
    [u,m,n] = unique(E,'rows');
    % determine counts for each unique edge
    counts = accumarray(n(:), 1);
    % extract edges that only occurred once
    O = u(counts==1,:);

    % resample W with a grid, THIS IS VERY SLOW if V is large
    [Xr,Yr,Wr] = griddata(X(:,1),X(:,2),UI,unique(X(:,1)),unique(X(:,2))');
    % find all points inside the mesh, THIS IS VERY SLOW if V is large
    IN = inpolygon(Xr,Yr,X(O,1),X(O,2));
    % don't draw points outside the mesh
    Wr(~IN) = NaN;

    [C0,hc] = contour(Xr,Yr,Wr,[0,0]);
    hc.LineColor = 'k';
    hc.LineWidth = 1.5;
    [C,hc] = contour(Xr,Yr,Wr,10);
    hc.LineColor = 'k';
end
function rP = FindRootAlongDir(ip,P,tet2,phi,th,iel)
    iv = tet2(iel,:);
    PHI = phi(iv);
    
    itet = tetP2(tet2,P,iel);
    fi0 = itet.fi(ip);
    dfi0 = itet.dfi(ip);
    dPHI0 = (dfi0.'*PHI).';
    PHI0 = fi0.'*PHI;
    
%     xfigure(56)
%     hp1 = plot(1,PHI0,'-ko');
%     f = NaN(40,1);
    
    searchDir = dPHI0;
    searchDir = searchDir/norm(searchDir);
    
    %% Compute optimal step length sl
    DD = -(PHI0./dPHI0*searchDir.')*searchDir; % Descent direction
%     tic
    np = 20;
    a = linspace(0,1,np)';
    h = repmat(ip,np,1)+repmat(a,1,3).*DD;
    fih = itet.fi(h);
    PHIih = fih.'*PHI;
    [~,ind] = min(abs(PHIih));
    sl = a(ind);
%     toc
    
    %%
    PHIi = PHI0;
    dPHIi = dPHI0;
    cc = 1;
    while abs(PHIi) > th        
        %% Newton
        DD = -(PHIi./dPHIi*searchDir.')*searchDir; % Descent direction
        ip = ip + DD*sl;
        fi = itet.fi(ip);
        dfi = itet.dfi(ip);
        PHIi = fi.'*PHI;
        dPHIi = (dfi.'*PHI).';
        
       
        
        %% Viz
%         xfigure(45)
%         plot3(ip(1),ip(2),ip(3),'ko','MarkerFaceColor','k')
%         
%         f(cc) = PHIi;        
%         hp1.XData = (1:cc).';
%         hp1.YData = f(~isnan(f));
%         drawnow
        %% Counter
        cc = cc +1;        
    end
    
%     xfigure(56)
%     hp1 = plot((1:cc-1),f(~isnan(f)),'-ko');
%     figure(56)
%     title('Root finding - Newton')
%     xlabel('Iteration')
%     ylabel('\phi')
%     xfigure(45)
%     plot3(ip(1),ip(2),ip(3),'ko','MarkerFaceColor','k')
    
    rP = ip;
%     1;
end
function tetNormal = TetElementNormal(faces3,phi,P)
    xnod = P(:,1); ynod = P(:,2); znod = P(:,3);
    edges = [faces3(:,[1:2]);faces3(:,[2:3]);faces3(:,[3,1])];
    edges = unique(sort(edges,2),'rows');
    cutEdgesInds = sum(phi(edges) > 0,2) == 1;
    cutEdges = edges(cutEdgesInds,:);
    u = phi(cutEdges);
    [~,k]=sort(u,2);
    cutEdgesS = cutEdges;
    for i=1:size(cutEdgesS,1)
        cutEdgesS(i,:) = cutEdges(i,k(i,:));
    end
    nPHIv = [xnod(cutEdgesS(:,2))-xnod(cutEdgesS(:,1)),...
         ynod(cutEdgesS(:,2))-ynod(cutEdgesS(:,1)),...
         znod(cutEdgesS(:,2))-znod(cutEdgesS(:,1))];
    tetNormal = mean(nPHIv,1); % Normal
    tetNormal = tetNormal/norm(tetNormal);

end
function [nodes,PX] = MergeAndOrientDataPoints(data, EE)
    data = data(~all(isnan(data),2),:);
    PX = data(:,[1:3]);
    
%     xfigure(45);
%     plot3(PX(:,1),PX(:,2),PX(:,3),'rx')
    
    pinds = ~isnan(data(:,end));
    einds = data(pinds,end);
    nP = size(einds,1);
    edges = [EE(einds,:),zeros(nP,1)];
    edges(1:2:nP,3)=1:nP/2;
    edges(2:2:nP,3)=1:nP/2;
    %%
    ind0 = zeros(nP/2,1); ind0(1:2) = 1:2;
    ind1 = zeros(nP/2,1); ind1(1) = 1;
    ed = edges(2,1:2);
    iv = zeros(nP/2,1); iv(1) = 2;
    
    for i = 2:nP/2-1
        ia = find(all(ismember(edges(:,1:2),ed),2));
        ia = setdiff(ia,iv);
        if length(ia) > 1; ia = ia(1); end; 
        ind1(i) = edges(ia,3);
        if mod(ia,2) == 0; s = -1; else; s = 1; end
        ed = edges(ia+s,1:2);
        iv(i) = ia+s;
        ind0(i+1) = ia+s;
    end
    ind1(end) = setdiff(1:length(ind1),ind1);
    
    
    %%
    PX0 = PX(pinds,:);
    PX0 = PX0(ind0,:);
    PX1 = PX(~pinds,1:3);
    PX1 = PX1(ind1,:);
    %%
    nodes = [1:length(ind0)+length(ind1)];
    PX = [PX0;PX1];
end
function [data, EE] = SurfacePoints(P, cutFaces, phi, tol, iel, Elements, tet)
%     global gc1 hPatchFace H
    
    edgesl = [1,2;2,3;1,3];
    EE = EdgeConnectivity(cutFaces);
    
    data = NaN(4*3,4);
    ifaceInd = 1;
    lo = 1;
    %% Find Cut Points along element edges
    for iface = cutFaces'      
       
        x6 = P(iface,1); y6 = P(iface,2); z6 = P(iface,3); 
        PHIf = phi(iface);
        cutEdgesInd = find(sum(PHIf(edgesl)>=0,2) > 0 & sum(PHIf(edgesl)>=0,2) < 2);
        
        cutEdgesIndG = (ifaceInd-1)*3+cutEdgesInd;
        
        % Get edge cut points
        rc = FindEdgePoints(PHIf,cutEdgesInd,tol);
        rc = rc(cutEdgesInd,:);
        
        nrc = size(rc,1);
        if nrc ~= 2
            error('Should be two points!');
        end
        
        %mid point
        rcm = mean(rc,1);
        r0 = rcm; %starting point
        
        % Get Normal. aka search direction
        % Note, this is critical for the convergence of the following
        % Newton method! Gradient direction gives optimal convergence.
        dfi0 = basetriP2.dfi(r0);
        dPHI0 = dfi0.'*PHIf;
        n = dPHI0/norm(dPHI0);
%         
        % Newtons method
        ri = FindSurfacePoint(r0,PHIf,n,tol);

        
        %% Interpolate onto 3D triangle
        Pr = [rc;ri];
        fi_Pr = basetriP2.fi(Pr);
        PX = [fi_Pr*x6,fi_Pr*y6,fi_Pr*z6];
        
        
        %% Draw parametric triangle
%         drawParametricTriangle()        
        %% Draw tet face
%         drawTetFace()
        
        %%
        up = lo+2;
        data(lo:up,:) = [ PX, [cutEdgesIndG;NaN]];
        lo = up+1;
        ifaceInd = ifaceInd+1;
    end
end
function ri = FindSurfacePoint(ri,PHI,n,tol)
    [fi,dfidr,dfids] = baseP2(ri);
    PHIi = fi*PHI;
    
%     xfigure(56)
%     hp1 = plot(1,PHIi,'-ko');
%     title('Root finding - Newton')
%     xlabel('Iteration')
%     ylabel('\phi')
%     f = NaN(40,1);
%     f(1) = PHIi;
%     cc = 2;
    while abs(PHIi) > tol
        dPHIi = [dfidr*PHI,dfids*PHI];
        ri = ri - (PHIi/(dPHIi*n(:)))*n(:).';
        [fi,dfidr,dfids] = baseP2(ri);
        PHIi = fi*PHI;
        
%         f(cc) = PHIi;
%         hp1.XData = (1:cc);
%         hp1.YData = f(~isnan(f)).';
%         drawnow
%         cc = cc +1; 
    end
%     xfigure(56)
%     hp1 = plot((1:cc-1),f(~isnan(f)),'-ko');
    
    %% If the Point is outside of the triangle
    if ~isInsideTri(ri)
%         error('Extensive curvature detected. Refine the mesh!')
        warning('Extensive curvature detected. Surface is reaching outside of current element.')
    end
    
end
function ri = FindRootOnEdge(edge,PHI,tol)
    rs = [0,0;1,0;0,1];
    rse = rs(edge,:);
    searchDir = rse(2,:)-rse(1,:);
    searchDir = searchDir/norm(searchDir);
    
    ri = rse(1,:);
    [~,ind] = sort(PHI(edge));
    ra = rse(ind(1),:); rb = rse(ind(2),:);
    [fi,dfidr,dfids] = baseP2(ri);
    PHIi = fi*PHI;
%     sprintf('Iteration | PHIi | ri |')
%     c = 1;
    while abs(PHIi) > tol
        dPHIi = [dfidr*PHI,dfids*PHI];
        ri = ri - (PHIi/(dPHIi*searchDir'))*searchDir;
        
        if ~isInsideTri(ri) % Bisect
            ri = (ra+rb)/2;
            [fi,dfidr,dfids] = baseP2(ri);
            PHIi = fi*PHI;
            if PHIi < 0
                ra = ri;
            else
                rb = ri;
            end
        else
            [fi,dfidr,dfids] = baseP2(ri);
            PHIi = fi*PHI;
        end
%         [fi,dfidr,dfids] = baseP2(ri);
%         PHIi = fi*PHI;
        
%         fprintf('%d | %9.6f | %9.6f |  %9.6f|\n',c,PHIi,ri(1), ri(2))
%         c = c+1;
    end
%     1;
end
function rc = FindEdgePoints(PHI,cutEdgesInd,tol)
    rc = NaN(3,2);
    
    if any(cutEdgesInd == 1)
        edge = [1,2];
        rc(1,:) = FindRootOnEdge(edge,PHI,tol);
    end
    if any(cutEdgesInd == 2)
        edge = [2,3];
        rc(2,:) = FindRootOnEdge(edge,PHI,tol);
    end
    if any(cutEdgesInd == 3)
        edge = [3,1];
        rc(3,:) = FindRootOnEdge(edge,PHI,tol);
    end
end

function drawTetFace()
%         fi_r0 = basetriP2.fi(r0);
%         Pri = [fi_r0*x6,fi_r0*y6,fi_r0*z6];
%         xfigure(45);
%         gc1 = gc1 +1;
%         if gc1 == 1
%             H.hpf = patch(x6(1:3),y6(1:3),z6(1:3),'c'); axis equal off tight; hold on;
%             H.hpf.FaceColor = 'b';
%             H.hpf.FaceAlpha = 0.1;
            
%             H.hpp = text(x6,y6,z6, cellstr(num2str(iface)), 'BackgroundColor','w');
%         else
%             H.hpf.XData = x6(1:3); 
%             H.hpf.YData = y6(1:3); 
%             H.hpf.ZData = z6(1:3); 
            
%             H.hpp.delete;
%             H.hpp = text(x6,y6,z6, cellstr(num2str(iface)), 'BackgroundColor','w');
%         end
%         hp = patch(x6(1:3),y6(1:3),z6(1:3),'c');
%         hp.FaceColor = 'none';
        
%         
%         text(x6(1:3),y6(1:3),z6(1:3),cellstr(num2str( iface(1:3) )),'BackgroundColor','w')
%         text(mean(x6),mean(y6),mean(z6),num2str(ifaceInd),'BackgroundColor','g')
%         text(mean(x6(cutEdges),2),mean(y6(cutEdges),2),mean(z6(cutEdges),2), strread(num2str(cutEdgesIndG'),'%s'), 'BackgroundColor','y')
%         plot3(PX(:,1),PX(:,2),PX(:,3),'xb')
%         if iel==8
%             plot3(Pri(:,1),Pri(:,2),Pri(:,3),'*b')
%             1;
%         end
%         if iel==9
%             plot3(Pri(:,1),Pri(:,2),Pri(:,3),'*g')
%             1;
%         end
end

function drawParametricTriangle()
    %         xfigure();
%         patch(rs(:,1),rs(:,2),'w'); axis equal off tight; hold on; view(2)
%         text(rs(:,1),rs(:,2),strread(num2str(1:3),'%s'),'BackgroundColor','w')
%         text(rs(:,1),rs(:,2)+[-.1;-.1;.1]*1,cellstr(num2str(sign(PHI(1:3)))),'BackgroundColor','w')
% %         text(mean(rr(cutEdges),2),mean(ss(cutEdges),2), strread(num2str(cutEdgesIndG'),'%s'), 'BackgroundColor','y')
%         plot(rc(:,1),rc(:,2),'b-x')
%         quiver(rcm(1),rcm(2),n(1),n(2),0.4)
%         plot(ri(1),ri(2),'xk')
end

function EE = EdgeConnectivity(cutFaces)
    EE = zeros(3*size(cutFaces,1),2);
    lo = 1;
    for i = 1:size(cutFaces,1)
        ifa = cutFaces(i,1:3);
        up = lo+2;
        EE(lo:up,:) = ifa([1:2;2:3;[3,1]]);
        lo = up+1;
    end
    EE = sort(EE,2); %Edge connectivity for all faces
end
function [fi,dfidr,dfids] = baseP2(rs)
    r = rs(1);
    s = rs(2);

    fi = [(1-r-s)*(1-2*r-2*s),r*(2*r-1),s*(2*s-1),4*r*(1-r-s),4*r*s,4*s*(1-r-s)];
    dfidr = [4*s+4*r-3,4*r-1,0,-4*(s+2*r-1),4*s,-4*s];
    dfids = [4*s+4*r-3,0,4*s-1,-4*r,4*r,-4*(2*s+r-1)];

end
function test = isInsideTri(ri)
    tol = eps*10;
    test = ri(1)>0-tol & ri(1)<1+tol &...
           ri(2)>0-tol & ri(2)<1+tol &...
           1-ri(1)-ri(2) > 0-tol;
end