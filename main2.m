clear
clc
close all

%% Required files
matlabDir = UpDir(pwd,2);
addpath(fullfile(matlabDir,'Mesh','Tet2MeshDev','Tet2Mesh'))
addpath(fullfile(matlabDir,'Mesh','Elements','P2Element'))
addpath(fullfile(matlabDir,'Mesh','Elements','Q8Element'))
addpath(fullfile(pwd,'TetP2CutSurface'))


%% Parameters
ne = 3;
R = 0.49;

global gc1 H
gc1 = 0;
H.c = 0;

%% P2 Tet mesh
x0 = 0; x1 = 1;
y0 = 0; y1 = 1;
z0 = 0; z1 = 1;
nxe = ne; nye = ne; nze = ne;

mesh = Tet2Mesh(x0,x1,y0,y1,z0,z1,nxe,nye,nze);

% vizP1Mesh(mesh.T(:,1:4),mesh.P);
% title('P1 elements')
% 
% xfigure; light
% vizP2Faces(mesh.faces,mesh.P,2);
% title('P2 elements')

%% Surface function
surfaceFunction = @(x,y,z) ((x-0).^2 + (y-0).^2 + (z-0).^2).^0.5-R; %Spehre

%% Extract surface
tet1 = mesh.T(:,1:4);
tet2 = mesh.T;
Elements = mesh.Elements;
P = mesh.P;
xnod = P(:,1); ynod = P(:,2); znod = P(:,3);
phi = surfaceFunction(P(:,1),P(:,2),P(:,3));
%%
tic
Elements = TetP2CutSurface(tet2,P,Elements,phi);
toc
Elements









%%

function dir = UpDir(dir,n)
    for i = 1:n
        dir = fileparts(dir);
    end
end