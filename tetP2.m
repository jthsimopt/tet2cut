classdef tetP2 < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        nodes
        P
    end
    
    properties (Access = private)
        volume_
        A_
        needUpdate_b_
        needUpdate_C_
        needUpdate_fi_
        needUpdate_dfi_
        
        X_
        X_old_
        b_old_
        C_old_
        fi_old_
        dfi_old_
    end
    properties (Dependent, Access = private)
        
        b_
        C_
        fim_
        fi_
        dfi_
    end
    
    methods
        
        function O = tetP2(tet,P,iel)
            O.nodes = tet(iel,:);
            O.P = P(O.nodes,:);
            
            xc = O.P(:,1);
            yc = O.P(:,2);
            zc = O.P(:,3);
            A = zeros(10);
            for i=1:10
                A(i,:) = [1,xc(i),yc(i),zc(i),xc(i)*yc(i),xc(i)*zc(i),yc(i)*zc(i),xc(i)^2,yc(i)^2,zc(i)^2];
            end
            O.A_ = A;
            O.needUpdate_C_ = true;
        end
        
        function vol = volume(O)
            if isempty(O.volume_)
                xc = O.P(:,1);
                yc = O.P(:,2);
                zc = O.P(:,3);
                v=cross([xc(2)-xc(1),yc(2)-yc(1),zc(2)-zc(1)],[xc(3)-xc(1),yc(3)-yc(1),zc(3)-zc(1)]);
                vol=abs(dot(v,[xc(4)-xc(1),yc(4)-yc(1),zc(4)-zc(1)]))/6;
                O.volume_ = vol;
            else
                vol = O.volume_;
            end
        end
        
        function b = get.b_(O)
            if O.needUpdate_b_
                X = O.X_;
                x = X(:,1); y = X(:,2);  z = X(:,3);
                x=x(:);y=y(:);z=z(:);
                b = [x*0+1,x,y,z,x.*y,x.*z,y.*z,x.^2,y.^2,z.^2];
                O.b_old_ = b;
                O.needUpdate_b_ = false;
            else
                b = O.b_old_;
            end
        end
        
        function C = get.C_(O)
            if O.needUpdate_C_
                C = O.A_\eye(10); % Constants per node
                O.C_old_ = C;
                O.needUpdate_C_ = false;
            else
                C = O.C_old_;
            end
        end
        
        function val = get.fi_(O)
            if O.needUpdate_fi_
                val = O.C_'*O.b_';
                O.fi_old_ = val;
                O.needUpdate_fi_ = false;
            else
                val = O.fi_old_;
            end
        end
        
        function val = get.dfi_(O)
            if O.needUpdate_dfi_
                X = O.X_;
                x = X(:,1); y = X(:,2);  z = X(:,3);
                x=x(:);y=y(:);z=z(:);
                dbdx = [x*0,x*0+1,x*0,x*0,y,z,x*0,2*x,0*x,0*x];
                dbdy = [x*0,x*0,y*0+1,x*0,x,x*0,z,0*x,2*y,0*z];
                dbdz = [x*0,x*0,y*0,z*0+1,0*x,x,y,0*x,0*y,2*z];
                fix = O.C_'*dbdx';
                fiy = O.C_'*dbdy';
                fiz = O.C_'*dbdz';
                val = [fix,fiy,fiz];
                
                O.dfi_old_ = val;
                O.needUpdate_dfi_ = false;
            else
                val = O.dfi_old_;
            end
        end
        
        function val = fi(O,X)
        	checkX(O,X);
            val = O.fi_;
        end
        
        function val = dfi(O,X)
            checkX(O,X);
            val = O.dfi_;
        end
        
        
    end
    
end

function checkX(O,X)
    if ~isequal(O.X_,X)
        O.X_ = X;
        O.needUpdate_b_ = true;
        O.needUpdate_fi_ = true;
        O.needUpdate_dfi_ = true;
    end
end

